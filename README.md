[![Build Status](https://travis-ci.org/aspera-non-spernit/cyrconv.svg?branch=master)](https://travis-ci.org/aspera-non-spernit/cyrconv)

# СЧЯСОПЏ.ҒLЁХ

CYRCONV is a funny faux cyrillic character mapper. By default it transliterates characters of a given input text to cyrillic (Lоок-Дlікё) counterparts, while remaining the readability of the text. Using .flex files CYRCON.FLEX can map any character set into a desired output set. 

**Note:** Only tested on ArchLinux. May not work on Windows. You may need to install terminal fonts for your system.

## Installation

### Installation 

There are two options available:

1. crates.io

**Note:** If you want to use the provided flex files, you have to manually download them from this repository and copy them
into a folder like ```/usr/local/etc/cyrconv/flex```.

```bash
$ cargo install cyrconv
```

2. Build from source

Build the program in the src/bin folder and move it to the destination folder.

**Note:** Make sure you save the ```flex``` files from the flex folder outside the project folder.
There is currently no installation routine and cyrconv does not look into /usr/local/etc/.. for flex files by default.

Example:

```bash
// compile a release version
$ cargo build --release cyrconv
$ strip target/release/cyrconv 
// copy into the user program folder
$ sudo cp target/release/cycronv /usr/local/bin
// make it executable
$ sudo chmod 755 /usr/local/bin/cyrconv
// copy flex files
$ sudo mkdir /usr/local/etc/cyrconv
$ sudo cp flex /usr/local/etc/cyrconv/
```

## General Usage

The program allows you to pipe a string to pass it as a single argument or as multiple arguments.
If you want to use the flex option you have to pass them them as the first argument.

There are several ways *Cyrconv* can be used.

**Note:** From version 0.4.0 all input strings must be enclosed in "<STRING>". The flex sub command comes after
the input string

### Help

*Cyrconv* now supports the ```help``` argument

```bash
$ cyrconv help
$ cyrconv -h
$ cyrconv --help
```

### Text passed as argument

You can pass the input text alone or with an optional flex file.
Examples:

```bash
$ cyrconv "Hello Friends"
$ cyrconv "How are you?" flex {..}/nko.flex
```

### Piping input

Piping (|) a source or reading from a file. 

```bash
$ echo "Hello Friend!" | cyrconv
$ echo "Hello Friends!" | cyrconv flex {..}/1337-lite.flex
```

### Reading a text file

The < operator is the first argument. The optional .flex file works as well.

Examples:

```bash
$ cyrconv < ../Documents/the_hunger_games.txt
$ cyrconv < ../Documents/the_hunger_games.txt flex {..}/braille.flex 
```
Reading a text file or command output also works through piping.

Example:

```bash
$ ls -la | cyrconv
$ printf "Hello World\nHow are you?" | cyrconv
$ less LICENSE.md | cyrconv flex {..}/1337-lite.flex 
```

### Reading from stdin

You can write a multiline text into the console. The command string ```:quit```
executes the mapping and quits the program.

Examples:

```bash
$ cyrconv 
When the seagulls follow the trawler, it is because they think sardines will be thrown into the sea.
Interesting...
:quit
Шнёп тнё ѕёаgцllѕ fоllош тнё тгашlёг, іт іѕ вёсацѕё тнёч тніпк ѕагdіпёѕ шіll вё тнгошп іпто тнё ѕёа.
Іптёгёѕтіпg...
$ cyrconv flex {..}/braille.flex 
Hello World.
How are you today?
:quit
⠓⠑⠇⠇⠕⠀⠺⠕⠗⠇⠙.
⠓⠕⠺⠀⠁⠗⠑⠀⠽⠕⠥⠀⠞⠕⠙⠁⠽?
```

### ' апd "

If you want to map a string that contains ' or " you have to escape the character or surround the string with the other one, that's not in the string.

Examples:

```bash
$ echo "What's up" | cyrconv
$ cyrconv "That's pretty \"cheap\"." flex {..}/braille.flex 
$ echo "If you want to map a string that contains \' or \" you have to escape the character \
or surround the string other one, that\'s not in the string." | cyrconv
```

**Note:**Escaping characters is not required via stdin.


## The Счгсопѵ.flex Simple mapper

The Simple Mapper takes a set of characters and maps it to a target character set of same length.

*Cyrconv.flex* profives default mapping set. The Simple Mapper can also be used with an optional .flex file,
as previous exmaples have shown.

A number of simple mapping sets are provided in the ```flex``` folder. You can create your own.

### Default Simple Mapper

The default mapping works without a .flex file.

> cyrconv sentence

Example:

```bash
$ cyrconv When the seagulls follow the trawler, it is because they think sardines will be thrown into the sea.
Шнеп тне ѕеаgцllѕ fоllош тне тгашlег, іт іѕ весацѕе тнеч тніпк ѕагdіпеѕ шіll ве тнгошп іпто тне ѕеа.   
```

#### Счгсопѵ.flёх

The flex option allows you to load a custom character mapping file.

> cyrconv flex {flex_file} sentence

Example:

```bash
$ cyrconv "When the seagulls follow the trawler, it is because they think sardines will be thrown into the 
sea" flex rot-13.flex
Jura gur frnthyyf sbyybj gur genjyre, vg vf orpnhfr gurl guvax fneqvarf jvyy or guebja vagb gur frn
```

### The .flex file

The flex file is a simple text file that consists of two lines.

The first line is a string of characters that matches expected input characters.

The second line is a string that consists of characters of the desired output character set.

Both lines must have the same character count.
 
## Nice to know

### Сігсцмflёх

Btw, the character calle СІЯСЦМҒLЁХ ' ˇ ' maps into the character called СДЯОП ' ^ '.

### Тнё Яцѕѕіап 1337

You can also combine different character sets. Keep in mind, that for each output there must be a flex file that provides the output as input.

Examples:

```bash
$ cyrconv "Hello my friend. Nastrowje." flex {..}/1337-lite.flex | cyrconv
Н3ll0 мч fг13п). П4ѕтг0шј3.
```

## Currently available flex files

If you cannot see all characters in this section, you might have to install additional terminal fonts. See the Wiki for your linux distro for further information.

### 1337-l1t3.fl3x

Wh3n th3 s34gulls f0ll0w th3 tr4wl3r, 1t 1s b3(4us3 th3y th1nk s4r)1n3s w1ll b3 thr0wn 1nt0 th3 s34.

### ⠃⠗⠁⠊⠇⠇⠑.⠋⠇⠑⠭ 

**Note:** This is the correct transliteration from Latin to Braille.

⠺⠓⠑⠝⠀⠞⠓⠑⠀⠎⠑⠁⠛⠥⠇⠇⠎⠀⠋⠕⠇⠇⠕⠺⠀⠞⠓⠑⠀⠞⠗⠁⠺⠇⠑⠗,⠀⠊⠞⠀⠊⠎⠀⠃⠑⠉⠁⠥⠎⠑⠀⠞⠓⠑⠽⠀⠞⠓⠊⠝⠅⠀⠎⠁⠗⠙⠊⠝⠑⠎⠀⠺⠊⠇⠇⠀⠃⠑⠀⠞⠓⠗⠕⠺⠝⠀⠊⠝⠞⠕⠀⠞⠓⠑⠀⠎⠑⠁.

### счгсопѵ.flёх

Шнёп тнё ѕёаgцllѕ fоllош тнё тгашlёг, іт іѕ вёсацѕё тнёч тніпк ѕагdіпёѕ шіll вё тнгошп іпто тнё ѕёа.

### ჳeorჳia.flex

WႬeი ჯႬe Ⴝeaჳullჷ follow ჯႬe ჯrawler, iჯ iჷ ხeეauჷe ჯႬeႸ ჯႬiიk ჷarძiიeჷ will ხe ჯႬrowი iიჯo ჯႬe ჷea.

### gͱεεκ.ϝιεϰ

Ψhεη ͳhε ϟεαgυιιϟ ϝοιιοω ͳhε ͳͱαωιεͱ, iͳ iϟ βεϛαυϟε ͳhεγ ͳhiηκ ϟαͱδiηεϟ ωiιι βε ͳhͱοωη iηͳο ͳhε ϟεα.

### nkߋ

Wߚen ߙߚe Seߡgߎlls fߋllߋW ߙߚe ߠrߡWler? ߊߙ ߊs ߕeߏߡߎse ߙߚeߌ ߙߚߊnk Sߡr߄ߊnes Wߊll ߕe ߙߚrߋWn ߊnߙߋ ߙߚe seߡ,

### ebg-13.syrk 

Jura gur frnthyyf sbyybj gur genjyre, vg vf orpnhfr gurl guvax fneqvarf jvyy or guebja vagb gur frn.

### ⵜⵊFⴷⵍⴷGⴼ.Fⵃⴺⵝ

Wⴼⴺⵍ ⵜⴼⴺ SⴺⴷGⵡⵃⵃS FⵙⵃⵃⵙW ⵜⴼⴺ ⵜRⴷWⵃⴺR, ⵊⵜ ⵊS BⴺⵎⴷⵡSⴺ ⵜⴼⴺⵖ ⵜⴼⵊⵍK SⴷRDⵊⵍⴺS Wⵊⵃⵃ Bⴺ ⵜⴼRⵙWⵍ ⵊⵍⵜⵙ ⵜⴼⴺ Sⴺⴷ.

### Zzz.flёх

Zzzz zzz zzzzzzzz zzzzzz zzz zzzzzzz, zz zz zzzzzzz zzzz zzzzz zzzzzzzz zzzz zz zzzzzz zzzz zzz zzz.

### _.flёх