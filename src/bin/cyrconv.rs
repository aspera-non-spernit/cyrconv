//#![forbid(unsafe_code)]
#[macro_use] extern crate clap;
#[macro_use] extern crate cyrconv;
extern crate libloading as lib;

use clap::{ ArgMatches };
use cyrconv::{
    consts,
    ext::{ self, LoadedPlugin, PluginConfig },
    io::{ stdin, load_flex_file },
    mapper::{ Mapper, Output, Simple }
};
use std::{ fs, path::PathBuf };

// to ext
fn plugin_dir() -> std::io::Result<Vec<PathBuf>> {
    let mut loadables = Vec::new();
    for entry in fs::read_dir("plugins")? {
        let dir = entry?;
        if dir.file_type().unwrap().is_file() && dir.file_name().into_string().unwrap().ends_with(".so") {
            loadables.push(dir.path());
        }
    }
    Ok(loadables)
}

fn load_plugins(loadables: Vec<PathBuf>) -> Vec<LoadedPlugin> {
    let mut loaded_libs: Vec<LoadedPlugin> = Vec::new();
    for path in loadables {
        let lib: lib::Library = call_dynamic!(path.to_str().unwrap());
        let config = fcall!(lib, b"config", PluginConfig);
        let ll = LoadedPlugin { library: lib, config };
        loaded_libs.push( ll );
    }
    loaded_libs
}

fn main() -> Result<(), Box<std::error::Error>> {
    // loading app & arguments
    let cli = load_yaml!("cli.yml");
    let matches = clap::App::from_yaml(cli).get_matches();

    // load plugins
    let loaded_plugins = load_plugins(plugin_dir().unwrap());

    // display welcome message
    let wlc_lib = &loaded_plugins[0].library;
    let msg: String = fcall!(wlc_lib, b"entry", String);
    println!("{}", msg);

    // checking source of input: stdin or arguments
    let input: Vec<String> = if !matches.is_present(consts::ARG_STRING) { stdin()? }
    else { vec![matches.value_of(consts::ARG_STRING).unwrap().to_string()] };

    // loading Simple Mapper with or w/o flex option
    let mapper = if matches.is_present(consts::SUBCOMMAND_FLEX) {
        let file_name = &matches.subcommand_matches(consts::SUBCOMMAND_FLEX).unwrap().value_of(consts::SUBCOMMAND_FILE_ARG);
        match load_flex_file( file_name.unwrap() ) {
            Ok(mappings) => { Simple::new(&mappings[0], &mappings[1]) },
            Err(e) => { println!("{}", e); Simple::default() }
        }
    } else { Simple::default() };

    // debug ?
    if matches.is_present(consts::FLAG_DEBUG) { println!("{:#?}", &mapper); println!("{:#?}", &loaded_plugins); }

    // output
    for line in input { println!("{}", mapper.output(&line) ); }
    Ok(())
}
