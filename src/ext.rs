/// The ```mod ext``` provides functionality to extend cyrconv through plugins
/// and an example Welcome plugin.
use serde::{ Deserialize };
use std::{ fs, path::PathBuf };

pub trait Config { fn config() -> PluginConfig; }
pub trait Call { fn entry() -> String; }
#[derive(Debug, Deserialize)]
pub enum Permission { Edit, Display, Read }

/// A general ```Plugin```
#[derive(Debug, Deserialize)]
pub struct PluginConfig {
    pub author: String,
    pub name: String,
    pub description: String,
    pub license: String,
    pub permissions: Vec<Permission>
}
#[derive(Debug)]
pub struct LoadedPlugin { pub library: lib::Library, pub config: PluginConfig }

#[macro_export]
macro_rules! call_dynamic {
    ($path: expr) => {
        {
            let lib: lib::Library = lib::Library::new($path).unwrap();
            lib
        }
    }
}

#[macro_export]
macro_rules! fcall {
    ($lib: ident, $f: expr, $rt: ty) => { //$f: function 
        unsafe {
            let func: lib::Symbol<unsafe extern fn() -> $rt > = $lib.get($f).unwrap();
            func()
        }
    }
}