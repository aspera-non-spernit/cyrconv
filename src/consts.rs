/// Constants

pub const FLAG_DEBUG: &str = "debug";
pub const ARG_STRING: &str = "STRING";
pub const SUBCOMMAND_FLEX: &str = "flex";
pub const SUBCOMMAND_PLUGIN: &str = "plugin";
pub const SUBCOMMAND_FILE_ARG: &str = "FILE";