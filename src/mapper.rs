use std::{ collections::HashMap };

pub trait Output<'a> { 
    type Input;
    fn output(&self, input: Self::Input) -> String;
}
pub trait Mapper {
    type Input;
    type Result;
    /// maps a single ```char``` to another one.
    /// The implementing Mapper must provide a HashMap<char, char>
    fn map(&self, c: Self::Input) -> Result<Self::Result, String>;
}

/// The ```Simple``` Mapper takes an set of input characters [a] and maps it to the character set [b]
#[derive(Debug)] pub struct Simple(HashMap<char, char>);

impl Simple {
    pub fn new(a: &str, b: &str) -> Self { Simple(a.chars().zip(b.chars()).collect()) }
}

impl Default for Simple {
    fn default() -> Self {
        Self::new (
            "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz 1234567890!\"§$%&/()=?,.-;:_'{[]}<>^|",
            "ДВСDЁҒGНІЈКLМПОРQЯЅТЦЏШХЧZавсdёfgніјкlмпорqгѕтцѵшхчz 1234567890!\"§$%&/()=?,.-;:_'{[]}<>ˇ|"
        )
    }
}
impl Mapper for Simple {
    type Input = char;
    type Result = char;
    fn map(&self, c: Self::Input) -> Result<char, String> {
        let out: char = *self.0.get(&c).unwrap_or(&c); 
        Ok(out) 
    }
}
impl <'a> Output<'a> for Simple {
    type Input = &'a str;
    fn output(&self, input: Self::Input) -> String {
        input.chars().map(|c| self.map(c).unwrap()).collect()
    }
}