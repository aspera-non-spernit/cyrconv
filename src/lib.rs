/// The cyrconv library
/// uses and consists of following dependencies.

extern crate libloading as lib;
#[macro_use] extern crate serde_derive;
extern crate serde;
extern crate serde_json;

pub mod consts;
pub mod ext;
pub mod io;
pub mod mapper;
