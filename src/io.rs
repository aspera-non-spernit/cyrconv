
/// ```mod io``` provides functionality facing input output operations facing
/// the user of cyrconv.

use std::{ fs::File, io::{ self, BufRead, BufReader, Error, Read } };
use serde::{ Deserialize };

pub fn stdin() -> io::Result<Vec<String>> {
    let stdin = io::stdin();
    let mut buffer: Vec<String> = Vec::new();
    let lines = stdin.lock().lines();
    for line in lines {
        if line.as_ref().unwrap() == ":quit" { break }
        buffer.push(line.unwrap());
    }
    Ok(buffer)
}

pub fn stdin_byte() -> Result<Vec<u8>, Error> {
    let mut stdin = io::stdin();
    let mut buffer = vec![0u8; 1024];
    stdin.read_to_end(&mut buffer)?;
    Ok(buffer)
}

pub fn load_flex_file(file_name: &str) -> Result<Vec<String>, String> {
    match File::open(file_name) {
        Ok(f) => {
            let reader = BufReader::new(f);
            let lines = reader.lines();
            let mut ln: Vec<String> = vec![];
            for line in lines {
                ln.push(line.unwrap());
            }
            Ok(ln)
        },
        Err(_e) => Err("The FlexMapper needs two lines in the flex file.".to_string())
    }
}